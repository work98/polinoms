﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Polinoms
{
    class Polinom
    {
        List<Monom> monoms = new List<Monom>();
        public Polinom(string equation)
        {
            var regexes_monoms = Regex.Matches(equation, @"([+-]?[^-+]+)");
                regexes_monoms.ToList().ForEach(monom =>
                {
                    char symbol = Regex.Match(monom.Value, @"([a-z])").Value[0];
                    var nums = Regex.Matches(monom.Value, @"([-+]\d+)");
                    int k = int.Parse(nums[0].Value);
                    int p = nums.Count == 2 ? int.Parse(nums[1].Value) : 1;
                    this.monoms.Add(new Monom { koeff = k, power = p, symbol = symbol });
                });
        }
        public void Squeeze()
        {
            List<Monom> monoms = new List<Monom>();
            this.monoms.ForEach(monom =>
            {
                var buff = monoms.FirstOrDefault(p => p.symbol == monom.symbol&& p.power == monom.power);
                if (buff!=null)
                {
                    buff.koeff += monom.koeff;
                } else
                {
                    monoms.Add(monom);
                }
            });
            this.monoms = monoms;
        }
    }
}
