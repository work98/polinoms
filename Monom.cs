﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Polinoms
{
    class Monom
    {
        public int koeff { get; set; }
        public int power { get; set; }
        public char symbol { get; set; }
    }
}
