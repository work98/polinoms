﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Polinoms
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите полином: ");
            var equation = Console.ReadLine();
            var polinom = new Polinom(equation);
            Console.WriteLine();
            polinom.Squeeze();
            Console.WriteLine();
            return;
        }
    }
}
